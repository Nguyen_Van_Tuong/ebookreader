package com.haianh.dto;

import android.os.Parcel;
import android.os.Parcelable;


import java.io.File;


public class FileObject implements Parcelable {
    private String filePath;
    private String fileName;
    private String baseDirectoryPath;
    private int fileID;

    public FileObject(File file) {
        this.filePath = file.getPath();
        this.fileName = file.getName();
        this.baseDirectoryPath =  file.getParentFile().getAbsolutePath();
        this.fileID = file.hashCode();
    }

    protected FileObject(Parcel in) {
        filePath = in.readString();
        fileName = in.readString();
        baseDirectoryPath = in.readString();
        fileID = in.readInt();
    }

    public static final Creator<FileObject> CREATOR = new Creator<FileObject>() {
        @Override
        public FileObject createFromParcel(Parcel in) {
            return new FileObject(in);
        }

        @Override
        public FileObject[] newArray(int size) {
            return new FileObject[size];
        }
    };

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getBaseDirectoryPath() {
        return baseDirectoryPath;
    }

    public int getFileID() {
        return fileID;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(filePath);
        dest.writeString(fileName);
        dest.writeString(baseDirectoryPath);
        dest.writeInt(fileID);
    }
}
