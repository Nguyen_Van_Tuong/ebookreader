package com.haianh.dto;

import android.os.Parcel;
import android.os.Parcelable;


public class PagePosition implements Parcelable{

    private int fileID;
    private double pagePosition;
    private int fontSize;
    private String fontType;

    public PagePosition() {
    }

    protected PagePosition(Parcel in) {
        fileID = in.readInt();
        pagePosition = in.readFloat();
        fontSize = in.readInt();
        fontType = in.readString();
    }

    public static final Creator<PagePosition> CREATOR = new Creator<PagePosition>() {
        @Override
        public PagePosition createFromParcel(Parcel in) {
            return new PagePosition(in);
        }

        @Override
        public PagePosition[] newArray(int size) {
            return new PagePosition[size];
        }
    };

    public int getFileID() {
        return fileID;
    }

    public void setFileID(int fileID) {
        this.fileID = fileID;
    }

    public double getPagePosition() {
        return pagePosition;
    }

    public void setPagePosition(double pagePosition) {
        this.pagePosition = pagePosition;
    }

    public int getFontSize() {
        return fontSize;
    }

    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
    }

    public String getFontType() {
        return fontType;
    }

    public void setFontType(String fontType) {
        this.fontType = fontType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(fileID);
        dest.writeDouble(pagePosition);
        dest.writeInt(fontSize);
        dest.writeString(fontType);
    }
}
