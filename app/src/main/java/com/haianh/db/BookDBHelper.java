package com.haianh.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.haianh.dto.FileObject;
import com.haianh.dto.PagePosition;


public class BookDBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "bookDev";

    public static final String PAGE_POSITION_TABLE = "page_position_table";

    public static final String FILE_ID = "FILE_ID", PAGE_POSITION = "PAGE_POSITION", FONT_TYPE = "FONT_TYPE", FONT_SIZE = "FONT_SIZE";

    public static final String BOOK_CODE = "BOOK_CODE", CODE = "CODE", CHAPTER_INDEX = "CHAPTER_INDEX", PAGE_POSITION_IN_BOOK = "PAGE_POSITION_IN_BOOK",
            PAGE_POSITION_IN_CHAPTER = "PAGE_POSITION_IN_CHAPTER", START_INDEX = "START_INDEX", END_INDEX = "END_INDEX", START_OFFSET = "START_OFFSET",
            END_OFFSET = "END_OFFSET", COLOR = "COLOR", TEXT_SELECTED = "TEXT_SELECTED", LEFT = "LEFT", TOP = "TOP",
            NOTE = "NOTE", IS_NOTE = "IS_NOTE", IS_OPEN = "IS_OPEN", FOR_SEARCH = "FOR_SEARCH",
            STYLE = "STYLE", PAGE_INDEX = "PAGE_INDEX";

    public static final String NUMBER_OF_CHAPTERS_IN_BOOK = "NUMBER_OF_CHAPTERS_IN_BOOK", NUMBER_OF_PAGES_IN_CHAPTER = "NUMBER_OF_PAGES_IN_CHAPTER", CHAPTER_TITLE = "CHAPTER_TITLE",
            HIGHLIGHTS_IN_PAGE = "HIGHLIGHTS_IN_PAGE", PAGE_DESCRIPTION = "PAGE_DESCRIPTION", DATE_TIME = "DATE_TIME", PAGE_INDEX_IN_BOOK = "PAGE_INDEX_IN_BOOK",
            NUMBER_OF_PAGES_IN_BOOK = "NUMBER_OF_PAGES_IN_BOOK", FIRST_CHARACTER_OFFSET_IN_PAGE = "FIRST_CHARACTER_OFFSET_IN_PAGE", TEXT_LENGTH_IN_PAGE = "TEXT_LENGTH_IN_PAGE";

    public String BOOKMARK_PAGE_INFO = "BOOKMARK_PAGE_INFO";

    public BookDBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + PAGE_POSITION_TABLE + "(" + FILE_ID + " integer, " + PAGE_POSITION + " real, " +
                "" + FONT_SIZE + " integer, " + FONT_TYPE + " text)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + PAGE_POSITION_TABLE);
        onCreate(db);
    }

    public int getRowCount(String tableName) {

        SQLiteDatabase sqLiteDatabase = getReadableDatabase();

        int numRows = (int) DatabaseUtils.queryNumEntries(sqLiteDatabase, tableName);

        return numRows;
    }

    /**
     * Method to delete all entries from table using DELETE FROM command
     *
     * @param tableName Name of table
     */
    public void truncateTable(String tableName) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        try {
            sqLiteDatabase.execSQL("DELETE FROM " + tableName);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            sqLiteDatabase.close();
        }

    }

    /**
     * To truncate all tables
     */
    public void truncateAll() {
        truncateTable(PAGE_POSITION_TABLE);
    }


    //INCLUDE PAGE_POSITION_TABLE
    public boolean isFileIDExists(FileObject fileObject) {
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();

        if (getRowCount(PAGE_POSITION_TABLE) != 0) {

            Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM " + PAGE_POSITION_TABLE, null);

            if (cursor.moveToFirst()) {
                do {
                    if (fileObject.getFileID() == cursor.getInt(cursor.getColumnIndex(FILE_ID))) {
                        return true;
                    }
                } while (cursor.moveToNext());
            }

            sqLiteDatabase.close();
        }

        return false;
    }

    public float insertNewIntoPagePositionTable(FileObject fileObject, double pagePosition, int fontSize, String fontType) {

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(FILE_ID, fileObject.getFileID());
        contentValues.put(PAGE_POSITION, pagePosition);
        contentValues.put(FONT_SIZE, fontSize);
        contentValues.put(FONT_TYPE, fontType);

        long rowID = sqLiteDatabase.insert(PAGE_POSITION_TABLE, null, contentValues);

        sqLiteDatabase.close();

        return rowID;
    }

    public int updateIntoPagePositionTable(FileObject fileObject, double pagePosition, int fontSize, String fontType) {

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(PAGE_POSITION, pagePosition);
        contentValues.put(FONT_SIZE, fontSize);
        contentValues.put(FONT_TYPE, fontType);

        return sqLiteDatabase.update(PAGE_POSITION_TABLE, contentValues, FILE_ID + "=?", new String[]{String.valueOf(fileObject.getFileID())});
    }

    public PagePosition readFromPagePositionTable(FileObject fileObject) {
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        PagePosition pagePosition = new PagePosition();

        Cursor findEntry = sqLiteDatabase.query(PAGE_POSITION_TABLE, null, FILE_ID + "=?", new String[]{String.valueOf(fileObject.getFileID())}, null, null, null);

        if (findEntry.getCount() != 0) {

            if (findEntry.moveToFirst()) {
                pagePosition.setFileID(findEntry.getInt(findEntry.getColumnIndex(FILE_ID)));
                pagePosition.setPagePosition(findEntry.getDouble(findEntry.getColumnIndex(PAGE_POSITION)));
                pagePosition.setFontSize(findEntry.getInt(findEntry.getColumnIndex(FONT_SIZE)));
                pagePosition.setFontType(findEntry.getString(findEntry.getColumnIndex(FONT_TYPE)));
            }

        }

        findEntry.close();

        sqLiteDatabase.close();

        return pagePosition;
    }

       private void showLog(String tag, String value) {
        if (tag.length() > 22) tag = tag.substring(0, 21);
        Log.d(tag, value);
    }
}
