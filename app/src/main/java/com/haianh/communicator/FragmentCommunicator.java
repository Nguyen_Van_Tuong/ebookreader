package com.haianh.communicator;

import java.util.List;


public interface FragmentCommunicator {

    void sendMessage(String fragmentIdentifier, List contentList);
}
