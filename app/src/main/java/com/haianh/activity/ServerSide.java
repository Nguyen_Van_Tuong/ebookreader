package com.haianh.activity;

import android.widget.Toast;


import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Enumeration;


class ServerSide {
    private BookReadActivity activity;
    private ServerSocket serverSocket;
    private String message = "";
    private static final int socketServerPORT = 8080;
    public static boolean isPaging = false;
    public static boolean isLoaded = false;

    ServerSide(BookReadActivity activity) {

        this.activity = activity;
        Thread socketServerThread = new Thread(new SocketServerThread());
        socketServerThread.start();

    }

    public int getPort() {
        return socketServerPORT;
    }

    void onDestroy() {
        if (serverSocket != null) {
            try {
                serverSocket.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            serverSocket = null;
        }
    }
    private class SocketServerThread extends Thread {

        int count = 0;

        @Override
        public void run() {
            try {
                serverSocket = new ServerSocket(socketServerPORT);
                while (true) {
                    Socket socket = serverSocket.accept();
                    count++;
                    message += "#" + count + " from "
                            + socket.getInetAddress() + ":"
                            + socket.getPort() + "\n";
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                           Toast.makeText(activity.getApplicationContext(), message,Toast.LENGTH_LONG).show();
                        }
                    });
                    OnPageChange onPageChange = new OnPageChange(socket);
                    onPageChange.run();
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    private class OnPageChange extends Thread {
        private Socket socket;
        OnPageChange(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            try {
                DataOutputStream sendToClient= new DataOutputStream(socket.getOutputStream());
                while (true){
                    if(!isLoaded) {
                        sendToClient.writeBytes(activity.selectedFileObject.getFileName()+"\n");
                        sendToClient.writeBytes(activity.pagePosition+"\n");
                        isLoaded =true;
                    }
                        if(isPaging ) {
                        sendToClient.writeBytes(activity.pagePosition+"\n");
                        isPaging = false;
                    }
                    if(socket.isClosed()) break;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    String getIpAddress() {
        String ip = "";
        try {
            Enumeration<NetworkInterface> enumNetworkInterfaces = NetworkInterface
                    .getNetworkInterfaces();
            while (enumNetworkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = enumNetworkInterfaces
                        .nextElement();
                Enumeration<InetAddress> enumInetAddress = networkInterface
                        .getInetAddresses();
                while (enumInetAddress.hasMoreElements()) {
                    InetAddress inetAddress = enumInetAddress
                            .nextElement();

                    if (inetAddress.isSiteLocalAddress()) {
                        ip += "Server running at : "
                                + inetAddress.getHostAddress();
                    }
                }
            }

        } catch (SocketException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            ip += "Something Wrong! " + e.toString() + "\n";
        }
        return ip;
    }

}
