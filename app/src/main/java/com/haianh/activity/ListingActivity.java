package com.haianh.activity;

import android.Manifest;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.haianh.utils.CustomPermissionRequestCode;
import com.haianh.R;
import com.haianh.dto.FileObject;
import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

public class ListingActivity extends AppCompatActivity  {

    File externalRootFile;

    private ArrayList<File> fileList;
    public static ArrayList<FileObject> fileObjects;
    private ProgressDialog progressDialog;

    private RelativeLayout noFilesFoundRelativeLayout;

    private GridView filesListView;
    private Toolbar toolbar;
    private BookListViewAdapter bookListViewAdapter;
    private SearchView searchView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing);
        initialiseView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.library_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.toolbar_refresh :
                fileList.clear();
                fileObjects.clear();
                getEpubFiles();
                break;
            case R.id.libSearch:
                toggleSearchbar();
        }
        return true;
    }

    private void toggleSearchbar() {
        if(searchView.getVisibility()==View.VISIBLE){
            searchView.setVisibility(View.INVISIBLE);

        }
        else searchView.setVisibility(View.VISIBLE);
    }

    private void initialiseView() {

        checkRequiredPermissions();
        searchView = (SearchView) findViewById(R.id.svTimkiem);
        externalRootFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath());
        progressDialog = new ProgressDialog(this);
        fileList = new ArrayList<>();
        fileObjects = new ArrayList<>();
        noFilesFoundRelativeLayout = (RelativeLayout) findViewById(R.id.noFilesFoundRelativeLayout);
        filesListView = (GridView) findViewById(R.id.filesListView);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("  Thư viện của tôi ");
        setSupportActionBar(toolbar);
        noFilesFoundRelativeLayout.setVisibility(View.INVISIBLE);
        fileObjects = getListFromSharedPreference(this);
        if(fileObjects.isEmpty()) {getEpubFiles();}
        else {
            bookListViewAdapter = new BookListViewAdapter(this,R.layout.bookgridview,fileObjects);
            filesListView.setAdapter(bookListViewAdapter);
            filesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        goToBookReadActivity(bookListViewAdapter.myArray.get(position));
                    }
            });
        }

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                bookListViewAdapter.getFilter().filter(newText);
                return false;
            }
        });
    }

    public void getEpubFiles() {

        class GetFileList extends AsyncTask<Void, Void, Boolean> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showProgressDialog(" Đang tìm kiếm ... ", false);
            }

            @Override
            protected Boolean doInBackground(Void... params) {
                ArrayList<File> files = getAllEpubFileList(externalRootFile);
                return files.size() != 0;
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                hideProgressDialog();
                if (aBoolean) {
                    setArrayAdapter(fileList);
                } else {
                    showNoFileFoundRelativeLayout();
                }
            }
        }

        GetFileList getFileList = new GetFileList();
        getFileList.execute();

    }

    private ArrayList<FileObject> getListFromSharedPreference(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences("MyLib",MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("myJson","");
        if(json.isEmpty()){
            return new ArrayList<>();
        }
        else {
            ArrayList<FileObject> fileObjects;
            Type type = new TypeToken<ArrayList<FileObject>>(){}.getType();
            fileObjects = gson.fromJson(json,type);
            return fileObjects;
        }
    }
    private void savedListSharedPreference(Context context,ArrayList<FileObject> fileObjects){
        SharedPreferences myLib = context.getSharedPreferences("MyLib",MODE_PRIVATE);
        SharedPreferences.Editor myLibEditor = myLib.edit();
        Gson gson = new Gson();
        String json = gson.toJson(fileObjects);
        myLibEditor.putString("myJson",json);
        myLibEditor.commit();
    }

    public String reNameBook(String bookName){
        return bookName.replaceAll("_","\u0020").replaceAll(".epub", "");
    }

    private void setArrayAdapter(ArrayList<File> fileList) {

        for (File file : fileList) {
            FileObject fileObject = new FileObject(file);
            fileObjects.add(fileObject);
        }
        savedListSharedPreference(this,fileObjects);
        bookListViewAdapter = new BookListViewAdapter(this,R.layout.bookgridview,fileObjects);
        filesListView.setAdapter(bookListViewAdapter);
        filesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                goToBookReadActivity(fileObjects.get(position));
            }
        });
    }

    public ArrayList<File> getAllEpubFileList(File dir) {
        File listFile[] = dir.listFiles();
        if (listFile != null && listFile.length > 0) {
            for (File aListFile : listFile) {
                if (aListFile.isDirectory()) {
                    getAllEpubFileList(aListFile);
                } else {
                    if (aListFile.getName().endsWith(".epub")) {
                        fileList.add(aListFile);
                    }
                }

            }
        }
        return fileList;
    }

    public void showNoFileFoundRelativeLayout() {
        noFilesFoundRelativeLayout.setVisibility(View.VISIBLE);
    }

    public void goToBookReadActivity(FileObject fileObject) {

        Intent bookReadIntent = new Intent(this, BookReadActivity.class);
        bookReadIntent.putExtra("SELECTED_FILE", fileObject);
        startActivity(bookReadIntent);
    }

    private void showProgressDialog( String message, boolean cancelable) {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(cancelable);
        progressDialog.show();
    }

    private void hideProgressDialog() {
        if (progressDialog.isShowing()) progressDialog.dismiss();
    }

    public void showLog(String tag, String value) {
        if(tag.length() > 22) {
            tag = tag.substring(0, 22);
        }
        Log.d(tag, value);
    }

    private void checkRequiredPermissions() {
        //checking self permissions
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
            //request for Internet permission
            requestInternetPermission();
        }

        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            //request for ReadExternal Storage
            requestReadExternalStoragePermission();
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH) != PackageManager.PERMISSION_GRANTED) {
            //request for Write External Storage
            requestBlueToothPermission();
        }
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_SETTINGS) != PackageManager.PERMISSION_GRANTED){
            //request for Brightness setting
            requestWritesettingPermission();
        }
    }

    private void requestWritesettingPermission() {
        if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_SETTINGS)) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_SETTINGS}, CustomPermissionRequestCode.WRITE_SETTING);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_SETTINGS}, CustomPermissionRequestCode.WRITE_SETTING);
        }
    }

    private void requestInternetPermission() {
        if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.INTERNET)) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET}, CustomPermissionRequestCode.INTERNET_PERMISSION);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET}, CustomPermissionRequestCode.INTERNET_PERMISSION);
        }
    }

    private void requestReadExternalStoragePermission() {
        if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.READ_EXTERNAL_STORAGE}, CustomPermissionRequestCode.READ_EXT_STORAGE);
        } else {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.READ_EXTERNAL_STORAGE}, CustomPermissionRequestCode.READ_EXT_STORAGE);
        }
    }

    private void requestBlueToothPermission() {
        if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.BLUETOOTH)) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.BLUETOOTH}, CustomPermissionRequestCode.BLUETOOTH);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.BLUETOOTH}, CustomPermissionRequestCode.BLUETOOTH);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == CustomPermissionRequestCode.INTERNET_PERMISSION) {
            if(grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                showLog("Internet Permission", "Added");
            } else {
                showLog("Internet Permission", "Not Added");
            }
            requestReadExternalStoragePermission();
        } else if(requestCode == CustomPermissionRequestCode.READ_EXT_STORAGE){
            if(grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                showLog("Read ext Permission", "Added");
            } else {
                showLog("Read ext Permission", "Not Added");
            }
            initialiseView();
        } else if (requestCode == CustomPermissionRequestCode.BLUETOOTH){
            if(grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                showLog("bluetooth permission", "Added");
            } else {
                showLog("bluetooth permission", "Not Added");
                finishActivity();
            }
            initialiseView();
        }else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void finishActivity() {
        this.finish();
    }

    private class BookListViewAdapter extends ArrayAdapter<FileObject> implements Filterable{

        ListingActivity context;
        List<FileObject>myArray ;
        private  ArrayList<FileObject> myLib = new ArrayList<>();

        FileObjFilter fileObjFilter = new FileObjFilter();
        int layoutId;

        BookListViewAdapter(@NonNull ListingActivity context, @LayoutRes int resource, @NonNull ArrayList<FileObject> objects) {
            super(context,resource,objects);
            this.context = context;
            this.layoutId = resource;
            this.myArray = objects;
            this.myLib.addAll(objects);
        }

        @NonNull
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {

            View v = convertView;
            ViewHolder holder = new ViewHolder();

            if(convertView==null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = inflater.inflate(layoutId, null);
                holder.text = (TextView) v.findViewById(R.id.txtItem);
                holder.img = (ImageView) v.findViewById(R.id.imgItem);
                v.setTag(holder);
            }
            else {
                holder = (ViewHolder) v.getTag();
            }
                FileObject book = myArray.get(position);
                holder.text.setText(reNameBook(book.getFileName()));
                holder.img.setImageResource(R.drawable.phat);
            return v;
        }
        @NonNull
        @Override
        public Filter getFilter() {
            return fileObjFilter;
        }
        private class FileObjFilter extends Filter {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                constraint = constraint.toString().toLowerCase();
                    final ArrayList<FileObject> nMyArray = new ArrayList<>();
                    for (final FileObject f : myLib) {
                        String bookName = reNameBook(f.getFileName()).toLowerCase();
                        if (bookName.contains(constraint)) {
                            nMyArray.add(f);
                        }
                    }
                    results.values = nMyArray;
                    results.count = nMyArray.size();

                return results;
            }
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                myArray = (List<FileObject>) results.values;
                notifyDataSetChanged();
                clear();
                for (FileObject f:myArray)
                {
                  add(f);
                }
                notifyDataSetInvalidated();
            }
        }
    }
    private static class ViewHolder {
        TextView text;
        ImageView img;
    }
}
