package com.haianh.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import android.os.AsyncTask;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import com.haianh.R;
import com.haianh.communicator.FragmentCommunicator;
import com.haianh.db.BookDBHelper;
import com.haianh.dto.FileObject;
import com.haianh.dto.PagePosition;
import com.skytree.epub.FixedControl;
import com.skytree.epub.Highlight;
import com.skytree.epub.NavPoints;
import com.skytree.epub.PageInformation;
import com.skytree.epub.PageMovedListener;
import com.skytree.epub.PageTransition;
import com.skytree.epub.ReflowableControl;
import com.skytree.epub.SelectionListener;
import com.skytree.epub.SkyProvider;
import com.skytree.epub.State;
import com.skytree.epub.StateListener;
import java.util.ArrayList;
import java.util.List;


public class BookReadActivity extends AppCompatActivity implements FragmentCommunicator, View.OnClickListener{

    private final int MAX_FONTSIZE = 25;
    private final int MIN_FONTSIZE = 15;

    private ReflowableControl reflowableControl;
    private int fontSize;
    private String fontType;
    public double pagePosition;
    private boolean selectionStartedByUser;
    private boolean showToolbar;
    private ServerSide serverSide;
    private RelativeLayout danhSachKinhLayout;
    private ListView lvDanhSachKinh;

    private BookDBHelper bookDBHelper;

    private ProgressDialog progressDialog;

    public FileObject selectedFileObject;

    private LinearLayout toolbarLinearLayout;

    private LinearLayout contentListingLayout, linearLayoutBtn, settingLayout;

    private RelativeLayout tocListRelativeLayout,readerRelativeLayout;
    private boolean tocLoaded;

    private ListView tocContentListView;

    private boolean bookLoadTask = false;
    public boolean isBookLoaded = false;
    private Toolbar toolbar;

    Switch swDongBo;
    private RadioButton radioSv;
    private RadioButton radioClient;
    private TextView txtIP;
    private SeekBar brightnessSeekBar, fontsizeSeekBar;
    private int sysBrightness = 0;
    private ClientSide clientSide;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_book_read);

        Intent callingIntent = getIntent();
        selectedFileObject = callingIntent.getParcelableExtra("SELECTED_FILE");
        if(selectedFileObject != null) {
            initialiseView();
        } else {
            showToast("Not Getting Selected File...");
        }

        lvDanhSachKinh.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                selectedFileObject = ListingActivity.fileObjects.get(i);
                if (bookDBHelper.isFileIDExists(selectedFileObject)) {

                    PagePosition position = bookDBHelper.readFromPagePositionTable(selectedFileObject);
                    if (position != null) {
                        pagePosition = position.getPagePosition();

                    } else {
                        pagePosition = 0;
                    }
                } else {
                    pagePosition = 0;
                }
                toolbar.setTitle(reNameBook(selectedFileObject.getFileName()));
                readerRelativeLayout.removeAllViewsInLayout();
                reflowableControl = null;
                readerRelativeLayout.addView(setUpReflowableController(ListingActivity.fileObjects.get(i).getFileName(),ListingActivity.fileObjects.get(i).getBaseDirectoryPath()));
                tocLoaded = false;
                readerRelativeLayout.refreshDrawableState();
                danhSachKinhLayout.setVisibility(View.INVISIBLE);
            }
        });


    }

    private void initialiseView() {



        bookDBHelper = new BookDBHelper(this);

        toolbarLinearLayout = (LinearLayout) findViewById(R.id.toolbarLinearLayout);
        linearLayoutBtn = (LinearLayout) findViewById(R.id.llbtn);
        danhSachKinhLayout = (RelativeLayout) findViewById(R.id.rlChonBoKinh);
        danhSachKinhLayout.setVisibility(View.INVISIBLE);
        lvDanhSachKinh = (ListView) findViewById(R.id.lvDsKinh);
        settingLayout = (LinearLayout) findViewById(R.id.settinglayout);

        ArrayList<String> fileNamesArrayList = new ArrayList<>();
        for (FileObject file : ListingActivity.fileObjects) {
            fileNamesArrayList.add(reNameBook(file.getFileName()));
        }
        final ArrayAdapter<String> fileNamesArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, fileNamesArrayList);
        lvDanhSachKinh.setAdapter(fileNamesArrayAdapter);
        EditText txtTimKiemBoKinh = (EditText) findViewById(R.id.txtTimKiemBoKinh);
        txtTimKiemBoKinh.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                fileNamesArrayAdapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
         swDongBo = (Switch) findViewById(R.id.swDongBo);


        txtIP = (TextView) findViewById(R.id.txtIP);
        txtIP.setEnabled(false);
        radioSv = (RadioButton) findViewById(R.id.radiobtnSv);
        radioClient = (RadioButton) findViewById(R.id.radiobtnClient);
        radioClient.setEnabled(false);
        radioSv.setEnabled(false);
        fontsizeSeekBar = (SeekBar) findViewById(R.id.seekbar_fontsize);
        fontsizeSeekBar.setMax(MAX_FONTSIZE-MIN_FONTSIZE);
        brightnessSeekBar = (SeekBar) findViewById(R.id.seekBar_brightness);
        swDongBo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b) {
                    radioClient.setEnabled(true);
                    radioClient.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                            if(b) txtIP.setEnabled(true);
                            else txtIP.setEnabled(false);
                        }
                    });
                    radioSv.setEnabled(true);
                }
                else {
                    radioClient.setEnabled(false);
                    radioSv.setEnabled(false);
                }
            }
        });
        Button btnXacNhan = (Button) findViewById(R.id.btnXacNhan);
        btnXacNhan.setOnClickListener(this);
        Button btnBack = (Button) findViewById(R.id.btnBack);
        btnBack.setOnClickListener(this);
        Button btnChonkinh = (Button) findViewById(R.id.btnChonkinh);
        btnChonkinh.setOnClickListener(this);
        Button btnChonpham = (Button) findViewById(R.id.btnChonpham);
        btnChonpham.setOnClickListener(this);
        ImageButton btnHome = (ImageButton) findViewById(R.id.btnHome);
        btnHome.setOnClickListener(this);
        ImageButton btnSetting = (ImageButton) findViewById(R.id.btnSetting);
        btnSetting.setOnClickListener(this);
        Button btnFontSize = (Button) findViewById(R.id.btnSize);
        Button btnFontSize1 = (Button) findViewById(R.id.btnSize1);
        btnFontSize1.setOnClickListener(this);
        btnFontSize.setOnClickListener(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(reNameBook(selectedFileObject.getFileName()));
        setSupportActionBar(toolbar);

        readerRelativeLayout = (RelativeLayout) findViewById(R.id.readerRelativeLayout);
        contentListingLayout = (LinearLayout) findViewById(R.id.contentListingLayout);
        tocListRelativeLayout = (RelativeLayout) findViewById(R.id.tocListRelativeLayout);

        tocContentListView = (ListView) findViewById(R.id.tocContentListView);
        progressDialog = new ProgressDialog(this);

        Drawable temp = ResourcesCompat.getDrawable(getResources(),R.drawable.texture,null);
        assert temp != null;

        initialiseBookVariableValues(selectedFileObject);

        readerRelativeLayout.addView(setUpReflowableController(selectedFileObject.getFileName(), selectedFileObject.getBaseDirectoryPath()));

        toggleToolBar();

    }

    private void initialiseBookVariableValues(FileObject fileObject) {
        if (bookDBHelper.isFileIDExists(fileObject)) {

            PagePosition pagePosition = bookDBHelper.readFromPagePositionTable(fileObject);

            if (pagePosition != null) {
                this.pagePosition = pagePosition.getPagePosition();
                this.fontSize = pagePosition.getFontSize();
                this.fontType = pagePosition.getFontType();
            } else {
                this.pagePosition = 0;
                this.fontSize = MIN_FONTSIZE;
                this.fontType = "TimesRoman";
            }
        } else {
            this.pagePosition = 0;
            this.fontSize = MIN_FONTSIZE;
            this.fontType = "TimesRoman";
            bookDBHelper.insertNewIntoPagePositionTable(fileObject, this.pagePosition, this.fontSize, this.fontType);
        }

        tocLoaded = false;

        showToolbar = false;
        fontsizeSeekBar.setProgress(fontSize-MIN_FONTSIZE);

    }

    private void toggleToolBar() {

        toolbarLinearLayout.setVisibility(View.VISIBLE);
        linearLayoutBtn.setVisibility(View.VISIBLE);

        class ToggleToolBar extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... params) {

                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                if(toolbarLinearLayout.getVisibility() == View.VISIBLE ) {
                    toolbarLinearLayout.setVisibility(View.INVISIBLE);
                }
                if (linearLayoutBtn.getVisibility()==View.VISIBLE) linearLayoutBtn.setVisibility(View.INVISIBLE);

            }
        }

        new ToggleToolBar().execute();

    }

    private ReflowableControl setUpReflowableController(String fileName, String baseDirectoryPath) {

        ServerSide.isLoaded = false;
        reflowableControl = new ReflowableControl(this);
        fileName = prepareFileNameWithWhiteSpaceReplacement(fileName);
        reflowableControl.setBookName(fileName);
        reflowableControl.setBaseDirectory(baseDirectoryPath);
        reflowableControl.setLicenseKey("0000-0000-0000-0000");
        reflowableControl.setDoublePagedForLandscape(false);
        reflowableControl.setFont(fontType, fontSize);
        reflowableControl.setLineSpacing(135); // the value is supposed to be percent(%).
        reflowableControl.setHorizontalGapRatio(0.25);
        reflowableControl.setVerticalGapRatio(0.1);
        reflowableControl.setPageTransition(PageTransition.Curl);
        reflowableControl.setFingerTractionForSlide(true);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT);
        reflowableControl.setLayoutParams(params);
        SkyProvider skyProvider = new SkyProvider();
        reflowableControl.setContentProvider(skyProvider);
        reflowableControl.setStartPositionInBook(pagePosition);
        reflowableControl.useDOMForHighlight(false);
        reflowableControl.setNavigationAreaWidthRatio(0.0f); // both left and right side.
        SelectionHandler selectionHandler = new SelectionHandler();
        reflowableControl.setSelectionListener(selectionHandler);
        PageMoveHandler pageMoveHandler = new PageMoveHandler();
        reflowableControl.setPageMovedListener(pageMoveHandler);
        StateHandler stateHandler = new StateHandler();
        reflowableControl.setStateListener(stateHandler);
       // reflowableControl.setBackgroundColor(16766784);


        return reflowableControl;
    }

    @NonNull
    private String prepareFileNameWithWhiteSpaceReplacement(String fileName) {

        return fileName.replaceAll("\\s", "%20");

    }
    public String reNameBook(String bookname){
        return bookname.replaceAll("_","\u0020").replaceAll(".epub", "").toUpperCase();
    }

    private void showProgressDialog(String title, String message, boolean cancelable) {
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(cancelable);
        progressDialog.show();
    }

    private void hideProgressDialog() {
        if (progressDialog.isShowing()) progressDialog.dismiss();
    }

    private void loadTOCList() {

        NavPoints tocNavPoints = reflowableControl.getNavPoints();

        List<String> tocList = new ArrayList<>();

        for(int chapterIndex = 0; chapterIndex < tocNavPoints.getSize(); chapterIndex++) {
            tocList.add(tocNavPoints.getNavPoint(chapterIndex).text);
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(BookReadActivity.this, android.R.layout.simple_list_item_1, tocList);

        tocContentListView.setAdapter(arrayAdapter);

        tocListRelativeLayout.setVisibility(View.VISIBLE);

        tocLoaded = true;

        tocContentListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                reflowableControl.gotoPageByNavPointIndex(position);
                hideContentListingLayout();
            }
        });

    }

    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.book_read_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                settingLayout.setVisibility(View.VISIBLE);
                break;
            case R.id.server:
                if(serverSide == null){
                    serverSide = new ServerSide(this);
                    Toast.makeText(BookReadActivity.this,serverSide.getIpAddress(),Toast.LENGTH_LONG).show();
                }
                else Toast.makeText(BookReadActivity.this,serverSide.getIpAddress(),Toast.LENGTH_LONG).show();
                break;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onPause() {
        super.onPause();
        bookDBHelper.updateIntoPagePositionTable(selectedFileObject, pagePosition, fontSize, fontType);
          }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onBackPressed() {
        bookDBHelper.updateIntoPagePositionTable(selectedFileObject, pagePosition, fontSize, fontType);
        if(contentListingLayout.getVisibility() == View.VISIBLE ||
                settingLayout.getVisibility() == View.VISIBLE ||
                danhSachKinhLayout.getVisibility() == View.VISIBLE) {
            contentListingLayout.setVisibility(View.INVISIBLE);
            settingLayout.setVisibility(View.INVISIBLE);
            danhSachKinhLayout.setVisibility(View.INVISIBLE);
            showToolbar = false;
        }      else {
            super.onBackPressed();
            finishActivity();
        }
    }

    @Override
    public void sendMessage(String fragmentIdentifier, List contentList) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnChonkinh:
                toolbarLinearLayout.setVisibility(View.INVISIBLE);
                linearLayoutBtn.setVisibility(View.INVISIBLE);
                showContentListing();
                break;
            case R.id.btnChonpham:
                bookDBHelper.updateIntoPagePositionTable(selectedFileObject, pagePosition, fontSize, fontType);
                danhSachKinhLayout.setVisibility(View.VISIBLE);
                break;
            case R.id.btnSize:
                setMaxFontSize();
                break;
            case R.id.btnSize1:
                setMinFontSize();
                break;
            case R.id.btnHome:
                onBackPressed();
                break;
            case R.id.btnSetting:
                setBrithness();
                settingLayout.setVisibility(View.VISIBLE);
                break;
            case R.id.btnBack:
                settingLayout.setVisibility(View.INVISIBLE);
                break;
            case R.id.btnXacNhan:
                setFontSizebySeekbar();
                setSynchro();
                settingLayout.setVisibility(View.INVISIBLE);
        }

    }

    private void setMinFontSize() {
        if(fontSize != MIN_FONTSIZE) {
            fontSize = MIN_FONTSIZE;
            fontsizeSeekBar.setProgress(fontSize-MIN_FONTSIZE);
            reflowableControl.setFontSize(fontSize);
            reflowableControl.reload();
        }
    }

    private void setMaxFontSize() {
        if(fontSize != MAX_FONTSIZE) {
            fontSize = MAX_FONTSIZE;
            fontsizeSeekBar.setProgress(fontSize-MIN_FONTSIZE);
            reflowableControl.setFontSize(fontSize);
            reflowableControl.reload();
        }
    }
    private void setBrithness() {
        int mode = -1;
        try {
            mode = Settings.System.getInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE);
            sysBrightness = Settings.System.getInt(getContentResolver(),Settings.System.SCREEN_BRIGHTNESS);
            brightnessSeekBar.setProgress(sysBrightness);
        } catch (Exception e) {
            Toast.makeText(this,e.toString(),Toast.LENGTH_SHORT).show();
        }
        if (mode == Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC) {
            brightnessSeekBar.setEnabled(false);
        }
        else {
            brightnessSeekBar.setEnabled(true);
            Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);
            brightnessSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                    try {
                        Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, i);
                        int br = Settings.System.getInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS);
                        WindowManager.LayoutParams lp = getWindow().getAttributes();
                        lp.screenBrightness = (float) br / 255;
                        getWindow().setAttributes(lp);

                    } catch (Exception e) {
                        Toast.makeText(BookReadActivity.this,e.toString(),Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });
        }
    }

    private void setSynchro() {
        if(radioSv.isChecked()&& radioSv.isEnabled()) {
            serverSide = new ServerSide(this);
            Toast.makeText(BookReadActivity.this,serverSide.getIpAddress(),Toast.LENGTH_LONG).show();
        } else if(serverSide != null) serverSide.onDestroy();

        if(radioClient.isChecked()&& radioClient.isEnabled()) {
            String svIP = txtIP.getText().toString() + "";
            clientSide = new ClientSide(BookReadActivity.this,svIP);
            clientSide.execute();
        } else if(clientSide != null) clientSide.onDestroyed();
    }


    private void setFontSizebySeekbar() {
        int size = fontsizeSeekBar.getProgress()+ MIN_FONTSIZE;
        if(size != this.fontSize) {
            this.fontSize = size;
            reflowableControl.setFontSize(this.fontSize);
            reflowableControl.reload();
        }
    }

    private void finishActivity() {
        if(serverSide!=null) serverSide.onDestroy();
        serverSide = null;
        if(clientSide!=null) clientSide.onDestroyed();
        clientSide = null;
        this.finish();
    }

    private void showContentListing() {
        contentListingLayout.setVisibility(View.VISIBLE);
        showTOCListRelativeLayout();
    }

    private void hideContentListingLayout() {
        contentListingLayout.setVisibility(View.INVISIBLE);
    }

    private void showTOCListRelativeLayout() { tocListRelativeLayout.setVisibility(View.VISIBLE);}

    public void DataRespond(String response) {
        if(response.contains(".epub")){
            for (FileObject f : ListingActivity.fileObjects) {
                if(f.getFileName().equals(response)) {
                    readerRelativeLayout.removeAllViewsInLayout();
                    reflowableControl = null;
                    readerRelativeLayout.addView(setUpReflowableController(f.getFileName(), f.getBaseDirectoryPath()));
                    toolbar.setTitle(reNameBook(f.getFileName()));
                    readerRelativeLayout.refreshDrawableState();
                }
            }
        }
        else{
            double pageNumber = Double.valueOf(response);
            Toast.makeText(BookReadActivity.this,response,Toast.LENGTH_LONG).show();
            reflowableControl.gotoPageByPagePositionInBook(pageNumber);
        }
    }

    private class SelectionHandler implements SelectionListener {

        @Override
        public void selectionStarted(Highlight highlight, Rect rect, Rect rect1) {
            selectionStartedByUser = true;
            showToolbar = false;
        }

        @Override
        public void selectionChanged(Highlight highlight, Rect rect, Rect rect1) {
            selectionStartedByUser = true;
            showToolbar = false;
        }

        @Override
        public void selectionEnded(Highlight highlight, Rect startRect, Rect endRect) {
            selectionStartedByUser = true;
            showToolbar = false;
        }

        @Override
        public void selectionCancelled() {
            if(selectionStartedByUser) {
                selectionStartedByUser = false;
            } else {
                if(showToolbar) {
                    showToolbar = false;
                    toggleToolBar();
                } else {
                    showToolbar = true;
                }
            }
        }
    }

    private class PageMoveHandler implements PageMovedListener {

        @Override
        public void onPageMoved(PageInformation pageInformation) {
            pagePosition = pageInformation.pagePositionInBook;
            ServerSide.isPaging = true ;
        }

        @Override
        public void onChapterLoaded(int i) {
            if(!tocLoaded) {
                loadTOCList();
            }
        }

        @Override
        public void onFailedToMove(boolean b) {

        }
    }

    private class StateHandler implements StateListener {

        @Override
        public void onStateChanged(State state) {

            if (state.equals(State.NORMAL)) {
                hideProgressDialog();

                if (!bookLoadTask) bookLoadTask = true;

            } else if (state.equals(State.LOADING)) {

            } else if (state.equals(State.ROTATING)) {

            } else if (state.equals(State.BUSY)) {

            }
        }
    }
}