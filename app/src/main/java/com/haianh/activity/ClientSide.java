package com.haianh.activity;

import android.os.AsyncTask;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;


public class ClientSide extends AsyncTask<Void,Void,Void> {


    BookReadActivity clientActivity;
    int clientPort = 8080;
    private String clientAddress;
    private String response;
    private Socket socket;


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Toast.makeText(clientActivity,"Connecting to host: " + clientAddress,Toast.LENGTH_LONG).show();
    }

    public ClientSide(BookReadActivity clientActivity, String clientAddress) {
        this.clientActivity = clientActivity;
        this.clientAddress = clientAddress;
    }


    @Override
    protected Void doInBackground(Void... voids) {
        try {
            socket = new Socket(clientAddress, clientPort);
            BufferedReader inFromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            while (true){
                response = inFromServer.readLine();
                publishProgress();
                if(response.contains(".epub"))
                    Thread.sleep(3000);
                if(socket.isClosed()) break;
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }
    @Override
    protected void onProgressUpdate(Void... voids) {
        clientActivity.DataRespond(response);
    }

    @Override
    protected void onPostExecute(Void Void) {
        super.onPostExecute(Void);
    }

    void onDestroyed(){
        if ( socket != null) {
            try {
                socket.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            socket = null;
        }
    }

    public BookReadActivity getClientActivity() {
        return clientActivity;
    }

    public void setClientActivity(BookReadActivity clientActivity) {
        this.clientActivity = clientActivity;
    }

    public int getClientPort() {
        return clientPort;
    }

    public void setClientPort(int clientPort) {
        this.clientPort = clientPort;
    }

    public String getClientAddress() {
        return clientAddress;
    }

    public void setClientAddress(String clientAddress) {
        this.clientAddress = clientAddress;
    }
}
